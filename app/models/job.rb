class Job < ApplicationRecord
  validates :title, :description, presence: true
  has_many :tags
end
